import { prettyPrintJson } from "pretty-print-json";
import copy from "copy-to-clipboard";
import gotpl from "gotpl";
import QaPool from "./pool";
console.log("version:", "2022-02-19 23:54:55");
let resultObj = {
  questions: [],
};
let resultJson = "";

const logtpl = `
<% if(logs.length) { %>
  <% for(var i=0, l=logs.length; i<l; ++i){ %>
  <div class="log-item">
    <span class="log-item-type-<%= logs[i].type %>">
    <%= logs[i].type %> 
    <%= logs[i].message %>: 
    </span>
    <%= logs[i].line %>
  </div>
  <% } %>
<% } %>
`;
const questiontpl = `
<div class="question">
  <% for(var i=0, l=questions.length; i<l; ++i){ %>
    <% var item = questions[i]; %>
    <div class="question-wrap">
      <div class="question-title"><%= i+1 %>. <%= item.title %>【<%=item.typeText %>】</div>
      <% if(item.type === 'radio'){ %>
        <div class="radio">
          <% for(var j=0, k=item.options.length; j<k; ++j){ %>
            <% var isCorrect = item.answer.indexOf(item.options[j].value) > -1 %>
            <label class="label">
              <input 
                type="radio" 
                name="q_<%= i+1 %>" 
                value="<%= item.options[j].value %>" 
                <% if(isCorrect){ %>
                  checked
                <% } %>
              />
              <%= item.options[j].value %>.
              <%= item.options[j].text %>
              <% if(item.answer.indexOf(item.options[j].value) > -1){ %>
                <span class="answer">(正确答案)</span>
              <% } %>
            </label>
          <% } %>
        </div>
        <% if(item.desc){ %>
          <div class="question-desc">
            解析：<%= item.desc %>
          </div>
        <% } %>
      <% }else if(item.type === 'checkbox'){ %>
        <div class="checkbox">
          <% for(var j=0, k=item.options.length; j<k; ++j){ %>
            <% var isCorrect = item.answer.indexOf(item.options[j].value) > -1 %>
            <label class="label">
              <input 
                type="checkbox"  
                name="q_<%= i+1 %>" 
                value="<%= item.options[j].value %>" 
                <% if(isCorrect){ %>
                  checked
                <% } %>
              />
              <%= item.options[j].value %>.
              <%= item.options[j].text %>
              <% if(isCorrect){ %>
                <span class="answer">(正确答案)</span>
              <% } %>
            </label>
          <% } %>
        </div>
        <% if(item.desc){ %>
          <div class="question-desc">
            解析：<%= item.desc %>
          </div>
        <% } %>
      <% } else if(item.type === 'tof'){ %>
        <div class="radio">
          <label class="label">
            <input
              type="radio"
              name="q_<%= i+1 %>"
              value="true"
              <% if(item.answer === true){ %>
                checked
              <% } %>
            />
            正确
          </label>
          <label class="label">
            <input
              type="radio"
              name="q_<%= i+1 %>"
              value="false"
              <% if(item.answer === false){ %>
                checked
              <% } %>
            />
            错误
          </label>
        </div>
        <% if(item.desc){ %>
          <div class="question-desc">
            解析：<%= item.desc %>
          </div>
        <% } %>
      <% }else if(item.type === 'input'){ %>
        <div class="input">
          <label class="label">
            <input type="text" name="q_<%= i+1 %>" value="<%= item.answer %>" />
          </label>
        </div>
        <% if(item.desc){ %>
          <div class="question-desc">
            解析：<%= item.desc %>
          </div>
        <% } %>
      <% } %>
    </div>
  <% } %>
</div>
`;

document.getElementById("btn").addEventListener("click", function () {
  // 用户输入的字符串
  let text = document.getElementById("text").value;
  // console.log(text);
  const pool = new QaPool(text);
  const { questions, logs } = pool.getQuestions();
  console.log(logs);
  resultObj.questions = questions;
  // console.log(resultObj);

  resultJson = JSON.stringify(resultObj);
  document.getElementById("json-preview").innerHTML =
    prettyPrintJson.toHtml(resultObj);

  document.getElementById("count").innerHTML = questions.length;

  document.getElementById("html-preview").innerHTML = gotpl.render(
    questiontpl,
    resultObj
  );

  document.getElementById("log-preview").innerHTML = gotpl.render(logtpl, {
    logs: logs,
  });
});

document.getElementById("copy").onclick = () => {
  copy(resultJson);
  alert("已复制到剪贴板");
};

document.getElementById("download").onclick = () => {
  let strResult = resultObj.questions.reduce((acc, cur) => {
    return `${acc}${JSON.stringify(cur)}\n`;
  }, "");

  const filename = `questions_${new Date().toLocaleDateString()}.json`;

  // Start file download.
  var blob = new Blob([strResult], { type: "text/plain" });
  var url = window.URL.createObjectURL(blob);
  var a = document.createElement("a");
  a.href = url;
  a.download = filename;
  a.click();
  window.URL.revokeObjectURL(url);
};
