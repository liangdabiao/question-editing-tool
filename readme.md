# 答题程序题目生成工具

配合答题小程序使用的题目生成工具。
目前支持的题目类型：
- 单选题
- 多选题
- 简答题
- 判断题

具体支持的格式具体请参看 `index.html` 中的输入框内容。

答题小程序源码: https://gitee.com/planb16/online-exam-weapp

答题小程序视频教程: https://space.bilibili.com/1889446517/channel/collectiondetail?sid=57246

## 开发&部署方式

### 初始化

**注意：Node.js 版本过低不能编译，建议 v14 或以上。**

```bash
npm install

# 或者
# yarn
```
### 调试
本项目使用 parcel 打包，运行下面命令将在 `http://localhost:1234/` 运行一个同步刷新的本地调试服务。

```bash
npm run start

# 或者
# yarn start
```

### 打包

```bash
npm run build

# 或者
# yarn build
```
其中的参数`--dist-dir deploy` 指定打包文件`deploy`目录；

参数 `--public-url /exam-tool/` 指定部署后的资源相对路径。

## 在线演示
作为演示，已经将 `--dist-dir` 中的代码部署了一份 https://playground.jslab.pro/exam-tool/

如果不能访问，国内也部署了一份到腾讯云 https://exam-tool-8gvx1k3a0d765359-1253194655.ap-shanghai.app.tcloudbase.com/

## 云开发数据库注意事项
题目和选项（不包含题号）相同的题目会根据 `md5` 去重。

- 云数据库需要建一个字段 `md5` 的唯一索引;
- 导入时选择 `Upsert：如果记录存在则更新，否则插入新记录`，就会自动忽略已添加的题目。



